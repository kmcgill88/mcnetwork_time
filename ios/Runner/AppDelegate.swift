import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    var channel: FlutterMethodChannel?
    var flutterViewController: FlutterViewController?
    let saveKey = "com.mcgilldevtech.mcnetwork_time.entries"
    let suiteName = "group.mcnetworktime"

    public var userDefaults: UserDefaults? {
        return UserDefaults(suiteName: suiteName)
    }


  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
      
      self.flutterViewController = self.window.rootViewController as? FlutterViewController
      channel = FlutterMethodChannel.init(name: "com.mcgilldevtech.mcnetworktime.widget", binaryMessenger: flutterViewController as! FlutterBinaryMessenger)
      initFlutterChannel()


    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    func initFlutterChannel() {
        channel?.setMethodCallHandler { [self] (call: FlutterMethodCall, result: FlutterResult) in
            if call.method == "getEntries" {
                var savedArray = userDefaults?.stringArray(forKey: self.saveKey)
                if savedArray == nil {
                    userDefaults!.set([], forKey: self.saveKey)
                }
                
                savedArray = userDefaults?.stringArray(forKey: self.saveKey)
                print(savedArray?.count)
                result(savedArray)
            } else {
                result(FlutterMethodNotImplemented)
            }
        }
    }
}
