//
//  mcnetworkTimeWidget.swift
//  mcnetworkTimeWidget
//
//  Created by Kevin McGill on 7/12/22.
//

import WidgetKit
import SwiftUI
import Intents

struct Provider: IntentTimelineProvider {
    let suiteName = "group.mcnetworktime"
    let saveKey = "com.mcgilldevtech.mcnetwork_time.entries"
    let path = PathMonitorConnectivityProvider()

    public var userDefaults: UserDefaults? {
        return UserDefaults(suiteName: suiteName)
    }

    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), connectionType: .wifi)
    }

    func getSnapshot(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date(), connectionType: .wifi)
        completion(entry)
    }

    func getTimeline(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []

        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        let entryDate = Calendar.current.date(byAdding: .second, value: 60, to: currentDate)!
        let currentConnectivityType = path.currentConnectivityType

        let entry = SimpleEntry(date: entryDate, connectionType: currentConnectivityType)
        entries.append(entry)
        
        var savedArray = userDefaults?.stringArray(forKey: saveKey)
        print("1")
        if savedArray == nil {
            print("2")
            userDefaults!.set([], forKey: saveKey)
        }
        savedArray = userDefaults?.stringArray(forKey: saveKey)


        if let entry = try? JSONEncoder().encode(entry), let asString = String(data: entry, encoding: .utf8) {
            print("3")
            savedArray?.append(asString)
            userDefaults?.set(savedArray, forKey: saveKey)
            userDefaults?.synchronize()
        } else {
            print("couldn't save")
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry, Codable {
    let date: Date
    let connectionType: ConnectivityType
    
    func statusFrom() -> String {
        switch self.connectionType {
        case .wifi:
          return "wifi"
        case .cellular:
          return "mobile"
        case .wiredEthernet:
          return "ethernet"
        case .none:
          return "none"
        }
      }
}

struct mcnetworkTimeWidgetEntryView : View {
    var entry: Provider.Entry
    let saveKey = "com.mcgilldevtech.mcnetwork_time.entries"
    public let suiteName = "group.mcnetworktime"

    public var userDefaults: UserDefaults? {
        return UserDefaults(suiteName: suiteName)
    }
    public func foo() -> Int {
        let array = userDefaults?.array(forKey: saveKey) ?? []
        return array.count
    }


    var body: some View {
        Text(entry.date, style: .time)
        Text("\(entry.statusFrom()) (\(foo()))")
    }
}

@main
struct mcnetworkTimeWidget: Widget {
    let kind: String = "mcnetworkTimeWidget"

    var body: some WidgetConfiguration {
        IntentConfiguration(kind: kind, intent: ConfigurationIntent.self, provider: Provider()) { entry in
            mcnetworkTimeWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("McNetwork Time")
        .description("Tracks which network your device uses most")
    }
}

struct mcnetworkTimeWidget_Previews: PreviewProvider {
    static var previews: some View {
        mcnetworkTimeWidgetEntryView(
            entry: SimpleEntry(date: Date(), connectionType: .wifi))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
