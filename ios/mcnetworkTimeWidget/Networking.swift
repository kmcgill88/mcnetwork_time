//
//  Networking.swift
//  mcnetworkTimeWidgetExtension
//
//  Created by Kevin McGill on 7/12/22.
//

import Foundation
import Network

public enum ConnectivityType: Codable {
  case none
  case wiredEthernet
  case wifi
  case cellular
}

@available(iOS 12, *)
public class PathMonitorConnectivityProvider: NSObject {

  private let queue = DispatchQueue.global(qos: .background)

  private var _pathMonitor: NWPathMonitor?

    override init() {
        super.init()
        ensurePathMonitor()
      }
    
  public var currentConnectivityType: ConnectivityType {
    let path = ensurePathMonitor().currentPath
    if path.status == .satisfied {
      if path.usesInterfaceType(.wifi) {
        return .wifi
      } else if path.usesInterfaceType(.cellular) {
        return .cellular
      } else if path.usesInterfaceType(.wiredEthernet) {
        // .wiredEthernet is available in simulator
        // but for consistency it is probably correct to report .wifi
        return .wiredEthernet
      }
    }
    return .none
  }


  private func ensurePathMonitor() -> NWPathMonitor {
    if (_pathMonitor == nil) {
      let pathMonitor = NWPathMonitor()
      pathMonitor.start(queue: queue)
      _pathMonitor = pathMonitor
    }
    return _pathMonitor!
  }
}
