import 'dart:async';
import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

const platformChannelName = "com.mcgilldevtech.mcnetworktime.widget";

enum ConnectivityType {
  none,
  wiredEthernet,
  wifi,
  cellular,
}

class SimpleEntry {
  SimpleEntry(this.date, this.connectionType);

  DateTime date;
  ConnectivityType connectionType;
}

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const MyHomePage(title: 'McNetwork Time'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const platform = MethodChannel(platformChannelName);
  late StreamSubscription<ConnectivityResult> subscription;

  ConnectivityResult connectivityResult = ConnectivityResult.none;
  String? count;
  String? wifiPercent;
  String? mobilePercent;
  String? nonePercent;

  Future<void> _check() async {
    var wifi = [];
    var mobile = [];
    var none = [];
    final entries = await platform.invokeMethod("getEntries") as List<Object?>;
    final _count = entries.length.toString();
    final entriesStrings = entries
        .whereType<String>()
        .toList();
    if (entriesStrings.isNotEmpty) {
      for (var i in entriesStrings) {
        print(i);
      }
      wifi =
          entriesStrings.where((element) => element.contains('wifi')).toList();
      mobile = entriesStrings
          .where((element) => element.contains('cellular'))
          .toList();
      none =
          entriesStrings.where((element) => element.contains('none')).toList();
    }
    var result = await Connectivity().checkConnectivity();
    setState(() {
      connectivityResult = result;
      count = _count;
      wifiPercent = ((wifi.length / entriesStrings.length)*100).toString();
      mobilePercent = ((mobile.length / entriesStrings.length)*100).toString();
      nonePercent = ((none.length / entriesStrings.length)*100).toString();
    });
  }

  @override
  void initState() {
    super.initState();
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        connectivityResult = result;
      });
    });
  }

  @override
  dispose() {
    super.dispose();
    subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Your connection state is:',
            ),
            Text(
              connectivityResult.toString(),
            ),
            const SizedBox(height: 30),
            Text('$count entries'),
            Text('Wifi $wifiPercent%'),
            Text('Mobile $mobilePercent%'),
            Text('None $nonePercent%'),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _check,
        tooltip: 'Check Network Type',
        child: const Icon(Icons.refresh),
      ),
    );
  }
}
